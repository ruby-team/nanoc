require 'gem2deb/rake/spectask'
require 'gem2deb/rake/testtask'

ENV["CI"] = "1"

EXCLUDED_TESTS = [
  'nanoc/test/filters/test_markaby.rb',
  'nanoc/test/filters/test_rainpress.rb',
  'nanoc/test/filters/test_coffeescript.rb',
]

Gem2Deb::Rake::TestTask.new(:test_nanoc) do |t|
  t.libs = ['nanoc/test', 'nanoc/lib', 'nanoc-core/lib', 'nanoc-cli/lib', 'nanoc-spec/lib']
  t.test_files = FileList['nanoc/test/**/*_spec.rb'] + FileList['nanoc/test/**/test_*.rb']-EXCLUDED_TESTS
end

ENV["RUBYLIB"] ||= ""
ENV["RUBYLIB"] += ":" + File.expand_path(File.dirname(__FILE__) + "/../nanoc-spec/lib")


subprojects = %w[
  nanoc
  nanoc-core
  nanoc-cli
  nanoc-live
  nanoc-external
  nanoc-checking
  nanoc-deploying
]
subprojects.each do |subproject|
  task "spec-#{subproject}" do |t|
    Dir.chdir(subproject) do |dir|
      puts "=> #{t.name}"
      begin
        mv 'lib',  'lib.off'
        ruby "-S", "rspec", "--exclude-pattern=spec/**/{gem,meta,manifest}_spec.rb"
      ensure
        mv 'lib.off',  'lib'
        puts
        puts "<= #{t.name}"
      end
    end
  end
end

task :default do
  subtasks = ["test_nanoc"] + subprojects.map { |p| "spec-#{p}" }
  failed = []
  subtasks.each do |t|
    begin
      Rake::Task[t].invoke
    rescue
      failed << t
    end
  end
  fail "Failed: #{failed.join(', ')}" unless failed.empty?
end
